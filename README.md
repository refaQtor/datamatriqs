a simple Qt gui to create semacode images  (just like the icon image for this project!) for string that I type in.  It depends on having 'dmtxwrite' installed.  So, this is basically a Linux only application.

see the [wiki page](https://bitbucket.org/refaQtor/datamatriqs/wiki/Home)
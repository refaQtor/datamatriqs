# -------------------------------------------------
# Project created by QtCreator 2009-03-27T05:33:10
# -------------------------------------------------
QT += webkit
TARGET = DataMatriQs
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \

HEADERS += mainwindow.h \

FORMS += mainwindow.ui
RESOURCES += barqoderData.qrc

OTHER_FILES += \
    resources/datamatriqs-url.png \
    resources/arrow-right.png \
    resources/help-about.png \
    resources/window-close.png

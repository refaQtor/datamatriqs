/******************************************************************
//Copyright (c) 2012, Shannon Mackey (RefaQtor@gmail.com)
//All rights reserved.

//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:

//    1. Redistributions of source code must retain the above copyright notice, this
//       list of conditions and the following disclaimer.

//    2. Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTemporaryFile>
#include <QDir>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindowClass)
{
    ui->setupUi(this);

    processOwner = new QObject();

    connect(ui->action_Quit,SIGNAL(triggered()),qApp,SLOT(quit()));
    connect(ui->action_About_Qt,SIGNAL(triggered()),qApp,SLOT(aboutQt()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::withDocsFilePath(QString fname)
{
    return QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + QDir::separator()+ fname;
}

void MainWindow::on_action_About_triggered()
{
    QMessageBox::about(this,"Qt is Qool", "A little tool to generate those\n Nifty DataMatrix Barcodes.\ncopyright: 2012, Shannon Mackey\n(RefaQtor@gmail.com)\nhttps://bitbucket.org/refaQtor/datamatriqs\n\nThis depends on having 'dmtxwrite'\n installed separately");
}

void MainWindow::on_action_Barcode_triggered()
{
    QString thetext(ui->lineEdit->text());
    QString thefilename(thetext + ".png");
    thefilename.replace("/","-");
    QString filetoencode = withDocsFilePath(thefilename);

    QFile file(withDocsFilePath("DataMatriQs.txt"));
    if (file.open(QIODevice::WriteOnly)) {
        file.write( thetext.toAscii() );
        file.close();
    }
    qDebug() << "outfile: " << filetoencode;
    QString program = "dmtxwrite";

    QStringList arguments;
    arguments << file.fileName() << " -o " << filetoencode;
    qDebug() << "args:" << arguments;
    QProcess *dmtxwrite = new QProcess(processOwner);
    if (dmtxwrite)
    {
        dmtxwrite->setWorkingDirectory(QDir::homePath());
        QFile outfile(filetoencode);
        if (outfile.open(QIODevice::ReadWrite))
        {
            dmtxwrite->setStandardOutputFile(filetoencode,QIODevice::WriteOnly);
            dmtxwrite->start(program, arguments);
            outfile.close();
        }
    }
sleep(2); // FIXME: this just allows time for the png file to flush to disk for reading on next line
    ui->label->setPixmap(QPixmap(filetoencode));
    ui->statusBar->showMessage(filetoencode);


}
